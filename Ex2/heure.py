heureIn = input()
M = int(input())
H = int(input())

heure = heureIn.split(':')
h = int(heure[0])
m = int(heure[1])

m += h * H
m /= M
m = int(m)
h = m // 60
m = m % 60
h = h % 24
print(str(h) + ':' + str(m))